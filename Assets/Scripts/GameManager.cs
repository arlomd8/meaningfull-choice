﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Text uang, reputasi, karyawan, time;
    
    void Start()
    {
        uang.text = "Uang : " + Res.instance.uang.ToString();
        reputasi.text ="Reputasi : " + Res.instance.rep.ToString();
        //karyawan.text = "Karyawan : " + Res.instance.karyawan.ToString();
        time.text = "Waktu pengiriman : " + Res.instance.time.ToString();
    }

    void Update()
    {
        uang.text = "Uang : " + Res.instance.uang.ToString();
        reputasi.text = "Reputasi : " + Res.instance.rep.ToString();
        //karyawan.text = "Karyawan : " + Res.instance.karyawan.ToString();
        time.text = "Waktu pengiriman : " + Res.instance.time.ToString();
    }
}
