﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobManager : MonoBehaviour
{
    private float timer;
    private bool canCount = true;
    private bool doOnce = false;
    private bool timerStart = false;
    public Text timerText;
    private int dapatUang, dapatRep, jumlahPegawai;

    void Start()
    {
        //timer = Res.instance.time;
    }

    void Update()
    {
        
        if (timerStart)
        {
            if (Res.instance.time >= 0.00f)
            {
                Res.instance.time -= Time.deltaTime;
            }

            if (Res.instance.time <= 0.0f)
            {
               
                Res.instance.time = 0.0f;
                Res.instance.uang += dapatUang;
                Res.instance.rep += dapatRep;
                timerStart = false;

            }
        }
    }

    public void JobMotor()
    {
        if (!timerStart && Res.instance.uang >= 20)
        {
            timerStart = true;
            Res.instance.time = 15;
            Res.instance.uang -= 20;
            dapatUang = 150;
            dapatRep = 5;
        }
       
    }

    public void JobMobil()
    {
        if (!timerStart && Res.instance.uang >= 100)
        {
            timerStart = true;
            Res.instance.time = 60;
            Res.instance.uang -= 100;
            dapatUang = 350;
            dapatRep = 15;
        }
    }

    public void JobTruk()
    {
        if (!timerStart && Res.instance.uang >= 200)
        {
            timerStart = true;
            Res.instance.time = 150;
            Res.instance.uang -= 200;
            dapatUang = 850;
            dapatRep = 45;
        }
    }


    public void JobKapal()
    {
        if (!timerStart && Res.instance.uang >= 500)
        {
            timerStart = true;
            Res.instance.time = 450;
            Res.instance.uang -= 500;
            dapatUang = 1300;
            dapatRep = 80;
        }
    }

    public void JobPesawat()
    {
        if (!timerStart && Res.instance.uang >= 1000)
        {
            timerStart = true;
            Res.instance.time = 900;
            Res.instance.uang -= 1000;
            dapatUang = 2000;
            dapatRep = 125;
        }
    }
}
